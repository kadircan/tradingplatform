﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradingPlatform.API.Controllers.Trade;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;
using TradingPlatform.API.Repositories;
using TradingPlatform.API.Services;

namespace TradingPlatform.Test
{
    [TestFixture]
   public class OrderControllerTest
    {
        private Mock<IOrderRepository> _orderRepository;
        private Mock<IExchangeService> _exchangeService;
        private OrderController _controller;

        [SetUp]
        public void Setup()
        {

            IEnumerable<Order> orders = new List<Order>() {
                new Order{Amount = 1 , OrderSide = OrderSide.Ask},
                new Order{Amount = 2 , OrderSide = OrderSide.Bid},
            };
            decimal price = 100;
            _orderRepository = new Mock<IOrderRepository>();
            _exchangeService = new Mock<IExchangeService>();
            _controller = new OrderController(_orderRepository.Object, _exchangeService.Object);

            _orderRepository
                .Setup(x => x.GetLastAsync(It.IsAny<OrderSide>(), It.IsAny<int>()))
                .Returns(Task.FromResult(orders));

            _exchangeService
                .Setup(x => x.GetPriceAsync(It.IsAny<Instrument>(), It.IsAny<CurrencyType>(), It.IsAny<decimal>()))
                .Returns(Task.FromResult(price));
             
        }


        [Test]
        public async Task Get_ReturnsBadRequest_WithWrongSide()
        {
            // Act
            IActionResult actionResult = await _controller.Get("wrong-text");

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(actionResult);
        }


        [Test]
        public async Task Get_ReturnsBsadRequest_WithWrongSide()
        {
            // Act
            IActionResult actionResult = await _controller.Get("ask");
            var result = ((IEnumerable<object>)((ApiResponse)((OkObjectResult)actionResult).Value).Result).ToList();

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(actionResult);
            Assert.IsNotNull((ApiResponse)((OkObjectResult)actionResult).Value);
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public async Task Post_ReturnsBsadRequest_WithInvalidModel()
        {
            //Arrange 
            OrderViewModel model = new OrderViewModel {
              TraderName = "testname"
            };
            // Act
            var actionResult = await _controller.Post(model);

             
        }
    }
}
