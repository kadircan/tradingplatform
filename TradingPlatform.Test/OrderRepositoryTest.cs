﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradingPlatform.API;
using TradingPlatform.API.Repositories;

namespace TradingPlatform.Test
{
    [TestFixture]
    public class OrderRepositoryTest
    {
        [Test]
        public async Task AddAsync_ShouldWrite_Database()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<TradeDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;

            IOrderRepository _repository = new OrderRepository(new TradeDbContext(options));

            await _repository.AddAsync(new API.Model.Order
            {
                TraderName = "TestUser",
                OrderSide = API.Model.Enum.OrderSide.Ask,
                Amount = 100
            });

            _repository.Save();

            //Act
            var results = await _repository.GetLastAsync(API.Model.Enum.OrderSide.Ask, 5);


            //Assert
            Assert.AreEqual(1, results.ToList().Count);
            Assert.AreEqual("TestUser", results.FirstOrDefault().TraderName);

        }

        [Test]
        public async Task GetLastAsync_ShouldRead_Database()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<TradeDbContext>()
                .UseInMemoryDatabase(databaseName: "reads_from_database")
                .Options;

            IOrderRepository _repository = new OrderRepository(new TradeDbContext(options));
            for (int i = 0; i < 10; i++)
            {
                await _repository.AddAsync(new API.Model.Order
                {
                    TraderName = "TestUser",
                    OrderSide = API.Model.Enum.OrderSide.Ask,
                    Amount = i,
                    CreatedAt = DateTimeOffset.Now
                });
            }
            _repository.Save();

            //Act
            var results = await _repository.GetLastAsync(API.Model.Enum.OrderSide.Ask, 5);


            //Assert
            Assert.AreEqual(5, results.ToList().Count);
            Assert.AreEqual(5, results.Where(x => x.Amount >= 5).Count());

        }
    }
}
