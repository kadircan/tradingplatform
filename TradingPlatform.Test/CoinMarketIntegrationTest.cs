﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;
using TradingPlatform.API.Services;

namespace TradingPlatform.Test
{
    [TestFixture]
    public class CoinMarketIntegrationTest
    {
 
        [Test]
        public async Task GetCurrencyById_ReturnsBitcoinDetail_WithBitcoin()
        {
            //Arrange
            ICoinmarketcapClient m_Sut = new CoinmarketcapClient();
            //Act
            Currency currency = await m_Sut.GetCurrencyByIdAsync(Instrument.Bitcoin);

            //Assert
            Assert.AreEqual(currency.Id,"bitcoin");
            Assert.AreEqual(currency.Symbol,"BTC");
            Assert.IsNull(currency.PriceConvert);
            Assert.IsNull(currency.MarketCapConvert);
            Assert.IsNull(currency.Volume24Convert);          
        }

        [Test]
        public async Task GetCurrencyById_ReturnsBitcoinDetailInEuro_WithBitcoinInEuro()
        {
            //Arrange
            ICoinmarketcapClient m_Sut = new CoinmarketcapClient();
            //Act
            Currency currency = await m_Sut.GetCurrencyByIdAsync(Instrument.Bitcoin,CurrencyType.EUR);

            Assert.AreEqual(currency.Id, "bitcoin");
            Assert.AreEqual(currency.Symbol, "BTC");
            Assert.IsNotEmpty(currency.PriceConvert);
            Assert.IsNotEmpty(currency.MarketCapConvert);
            Assert.IsNotEmpty(currency.Volume24Convert);
            Assert.AreEqual(currency.ConvertCurrency, "EUR");        
        }

       
    }
}
