﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model;

namespace TradingPlatform.API
{
    public class TradeDbContext : DbContext
    {
       
            public TradeDbContext(DbContextOptions<TradeDbContext> options)
                : base(options)
            {
            }

            public DbSet<Order> Orders { get; set; }
               
    }
}
