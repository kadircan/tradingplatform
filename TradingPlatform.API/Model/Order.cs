﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Model
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }
        public CurrencyType Currency { get; set; }
        public Instrument Instrument { get; set; }
        public string TraderName { get; set; }
        public OrderSide OrderSide { get; set; }
        public decimal Amount { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }
}
