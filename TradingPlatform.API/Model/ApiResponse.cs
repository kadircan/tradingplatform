﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TradingPlatform.API.Model
{
    public class ApiResponse
    {
        public static ApiResponse Create(HttpStatusCode statusCode, object result = null, ApiError error = null)
        {
            return new ApiResponse(statusCode, result, error);
        }

        public string Version => "1.0.0";

        public int StatusCode { get; set; }
        public string RequestId { get; }

        public ApiError Error { get; set; }

        public object Result { get; set; }

        protected ApiResponse(HttpStatusCode statusCode, object result = null, ApiError error = null)
        {
            RequestId = Guid.NewGuid().ToString();
            StatusCode = (int)statusCode;
            Result = result;
            Error = error;
        }
    }
}
