﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Attributes;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Model
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }


        [RequiredEnum]
        public OrderSide OrderSide { get; set; }
     
        [Required]
        public string TraderName { get; set; }

        [GreaterThen(0, ErrorMessage = "Order amount must be greater then 0")]
        public decimal Amount { get; set; }

    
        public decimal Price { get; set; }

    }
   
}
