﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradingPlatform.API.Model.Enum
{
    public enum CurrencyType
    {
        USD,
        EUR,
        AUD,
        BRL,
        CAD,
        CHF,
        CNY,
        GBP,
        HKD,
        IDR,
        INR,
        JPY,
        KRW,
        MXN,
        RUB
    }
}
