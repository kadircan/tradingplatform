﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TradingPlatform.API.Model.Enum
{
    public enum OrderSide
    {
        None,
        Bid,
        Ask
    }
}
