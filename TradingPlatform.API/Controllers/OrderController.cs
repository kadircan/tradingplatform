﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using TradingPlatform.API.Model;
using System.Net;
using TradingPlatform.API.Repositories;
using TradingPlatform.API.Infrastructure;
using TradingPlatform.API.Services;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Controllers.Trade
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IExchangeService _exchangeService;

        public OrderController(IOrderRepository orderRepository,
            IExchangeService exchangeService)
        {
            _orderRepository = orderRepository;
            _exchangeService = exchangeService;
        }

        // GET: api/Order/ask
        [HttpGet("{side}")]
        public async Task<IActionResult> Get(string side)
        {

            OrderSide orderSide = OrderSide.Ask;
            if (!Enum.TryParse(side, true, out orderSide)) return BadRequest();

            //get last 5 order by side
            var orders = await _orderRepository.GetLastAsync(orderSide, 5);

            //create model
            var model = orders.Select(x =>
                new OrderViewModel
                {
                    Id = x.Id,
                    TraderName = x.TraderName,
                    OrderSide = x.OrderSide,
                    Amount = x.Amount,
                    Price = _exchangeService.GetPriceAsync(Model.Enum.Instrument.Bitcoin, Model.Enum.CurrencyType.EUR, x.Amount).GetAwaiter().GetResult()
                }
            ).ToList();

            //TODO: create response wrapper 
            return Ok(ApiResponse.Create(HttpStatusCode.OK, model));
        }


        // POST: api/Order
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody]OrderViewModel model)
        {
            //TODO: get instrument and currency from route  - POST: api/Order/btc/euro
            Order order = new Order
            {
                TraderName = model.TraderName,
                OrderSide = model.OrderSide,
                Amount = model.Amount,
                CreatedAt = DateTimeOffset.Now,
                Instrument = Instrument.Bitcoin,
                Currency = CurrencyType.EUR
            };

            await _orderRepository.AddAsync(order);
            var id = _orderRepository.Save();

            return Ok(ApiResponse.Create(HttpStatusCode.OK, model));
        }


    }
}
