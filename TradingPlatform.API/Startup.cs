﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using TradingPlatform.API.Repositories;
using TradingPlatform.API.Infrastructure;
using AutoMapper;
using TradingPlatform.API.Model;
using TradingPlatform.API.Services;
using Microsoft.Extensions.Caching.Memory;

namespace TradingPlatform.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<TradeDbContext>(options =>
            {
                // Use an in-memory database with a randomized database name (for testing)
                options.UseInMemoryDatabase();
            });
  
            services.AddMvc();
            services.AddMemoryCache();

            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IExchangeService, ExchangeService>();
            services.AddSingleton<ICoinmarketcapClient, CoinmarketcapClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<Order, OrderViewModel>().ReverseMap();
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var dbContext = app.ApplicationServices.GetService<TradeDbContext>();
            AddTestData(dbContext);

           

            // Serialize all exceptions to JSON
            var jsonExceptionMiddleware = new JsonExceptionMiddleware(
                app.ApplicationServices.GetRequiredService<IHostingEnvironment>());
            app.UseExceptionHandler(new ExceptionHandlerOptions { ExceptionHandler = jsonExceptionMiddleware.Invoke });

            //app.UseResponseWrapper();

            app.UseMvc();
        }

        private void AddTestData(TradeDbContext dbContext)
        {
            dbContext.Orders.Add(new Model.Order
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTimeOffset.UtcNow,
                Amount = 100,
                OrderSide = Model.Enum.OrderSide.Ask,
                TraderName = "testUser1",
                Currency = Model.Enum.CurrencyType.EUR,
                Instrument = Model.Enum.Instrument.Bitcoin

            });

             dbContext.Orders.Add(new Model.Order
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTimeOffset.UtcNow,
                Amount = 100,
                OrderSide = Model.Enum.OrderSide.Bid,
                TraderName = "testUser2",
                Currency = Model.Enum.CurrencyType.EUR,
                Instrument = Model.Enum.Instrument.Bitcoin
             });


            dbContext.SaveChanges();
        }
    }
}
