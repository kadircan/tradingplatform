﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Services
{
    public interface IExchangeService
    {
        Task<decimal> GetPriceAsync(Instrument instrument, CurrencyType currency, decimal amount);
    }

    public class ExchangeService : IExchangeService
    {

        private ICoinmarketcapClient _coinMarketClient;
        private IMemoryCache _cache;

        public ExchangeService(IMemoryCache cache,
            ICoinmarketcapClient coinMarketClient)

        {
            _coinMarketClient = coinMarketClient;
            _cache = cache;
        }


        public async Task<decimal> GetPriceAsync(Instrument instrument, CurrencyType currencyType, decimal amount)
        {
            //get bitcoin rate
            var cachekey = $"{instrument.ToString()}_{currencyType.ToString()}";

            Currency currency;
            // Look for cache key.
            if (!_cache.TryGetValue(cachekey, out currency))
            {
                // Key not in cache, so get data.
                currency = await _coinMarketClient.GetCurrencyByIdAsync(instrument, currencyType);

                // Set cache options.
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Keep in cache for this time, reset time if accessed.
                    .SetSlidingExpiration(TimeSpan.FromSeconds(1));

                // Save data in cache.
                _cache.Set(cachekey, currency, cacheEntryOptions);
            }
             
            //calculate value
            return ConvertAmount(amount, Convert.ToDecimal(currency.PriceConvert));
        }

        private decimal ConvertAmount(decimal amount, decimal price)
        {
            return amount * price;
        }
    }



}
