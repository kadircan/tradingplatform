﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Services
{
    public interface ICoinmarketcapClient
    {
        Task<Currency> GetCurrencyByIdAsync(Instrument id);
        Task<Currency> GetCurrencyByIdAsync(Instrument id, CurrencyType convertCurrency);
    }



}
