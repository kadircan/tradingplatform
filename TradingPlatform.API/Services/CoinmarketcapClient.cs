﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Services
{
    public class CoinmarketcapClient : ICoinmarketcapClient
    {
        private const string Url = "http://api.coinmarketcap.com";
        private const string Path = "/v1/ticker";

        public Task<Currency> GetCurrencyByIdAsync(Instrument id)
        {
            return Task.FromResult(CurrencyById(id.ToString(), string.Empty));
        }

        public Task<Currency> GetCurrencyByIdAsync(Instrument id, CurrencyType convertCurrency)
        {
            return Task.FromResult(CurrencyById(id.ToString(), convertCurrency.ToString()));
        }

        private Currency CurrencyById(string id, string convertCurrency)
        {
            string path = "/" + id;
            if (!string.IsNullOrEmpty(convertCurrency))
                path += "/?convert=" + convertCurrency;

            var client = new WebApiClient(Url);
            var result = client.MakeRequest(Path + path, Method.GET, convertCurrency);

            return result.First();
        }
    }
}
