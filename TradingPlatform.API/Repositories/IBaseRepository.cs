﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TradingPlatform.API.Repositories
{
    public interface IBaseRepository<T>
    {
        Task<IEnumerable<T>> GetAsync();
        Task<T> AddAsync(T entity);
        int Save();
    }   
}