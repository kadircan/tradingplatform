﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly TradeDbContext _context;
        
        public OrderRepository(TradeDbContext context)
        {
            _context = context;
        }

        public async Task<Order> AddAsync(Order order)
        {
             await _context.Orders.AddAsync(order);
            return order;
        }
         
        public async Task<IEnumerable<Order>> GetAsync()
        {
            return await _context.Orders.ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetLastAsync(OrderSide orderSide, int limit)
        {
            return await _context.Orders.Where(x=>x.OrderSide == orderSide)
                                        .OrderByDescending(x => x.CreatedAt)
                                        .Take(limit).ToListAsync();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
