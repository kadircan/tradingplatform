﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TradingPlatform.API.Attributes
{
    public class GreaterThenAttribute :  ValidationAttribute
    {
        public decimal MinLength { get; private set; }
        public GreaterThenAttribute(int minLength)
        {
            MinLength = minLength;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (Convert.ToDecimal(value) <= this.MinLength)
            {
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }
    }
}
