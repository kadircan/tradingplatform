﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TradingPlatform.API.Model.Enum;

namespace TradingPlatform.API.Attributes
{
    public class RequiredEnumAttribute :  ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            OrderSide side;
            if (Enum.TryParse<OrderSide>(value.ToString(), out side))
            {
                if (side == OrderSide.None)
                {
                    return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
                }              
            }
            return null;
        }
    }
}
